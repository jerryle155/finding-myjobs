const express = require('express');
const router = express.Router();

// GET
router.get('/', (req, res, next) => {
    res.render('candidate/index', { title: 'Candidate Page'});
});

module.exports = router;