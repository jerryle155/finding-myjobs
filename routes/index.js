const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');

// models
const Category = require('../models/category');
const Job = require('../models/job');
const User = require('../models/user');
const Role = require('../models/role');
const {
  forwardAuthenticated,
  ensureAuthenticated
} = require('../helpers/auth');


/* GET home page. */
router.get('/', function (req, res, next) {
  // console.log(req.user);
  Category.find({})
    .exec((err, categories) => {
      if (err) {
        console.log(err);
        return;
      }
      Category.find({}).then(() => {
        Job.find({})
          .sort('-createdAt')
          .limit(5)
          .populate('category')
          // .count({"category.name":cat.name})
          .exec((err, recentJobs) => {

            if (err) {
              console.log(err);
              return;
            } else {
              console.log(categories);
              res.render('index', {
                title: 'Finding MyJobs',
                categories: categories,
                jobs: recentJobs,
                user: req.user,
                // count
              });
            }
          });
      });
    });



});

router.get('/search', (req, res, next) => {
  const {title,category} = req.body;

  res.render('pages/search', {

  });
})

// router.get('/home', ensureAuthenticated, (req, res) => {
//   res.render('home', {
//     user: req.user
//   })
// });

// authentication
router.get('/login', forwardAuthenticated, (req, res, next) => {
  res.render('auth/login');
});

router.post('/login', (req, res, next) => {
  passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true
  })(req, res, next);
});

router.get('/signup', forwardAuthenticated, (req, res, next) => {
  Role.find({}).then(roles => {
    res.render('auth/register', {
      roles
    });
  });
  // res.render('auth/register');
});

router.post('/signup', (req, res) => {
  const {
    role,
    username,
    password,
    password2
  } = req.body;
  let errors = [];

  if (!username || !password || !password2 || !role) {
    errors.push({
      msg: 'Please enter all fields'
    });
  }

  if (password != password2) {
    errors.push({
      msg: 'Passwords do not match'
    });
  }

  if (errors.length > 0) {
    Role.find({}).then(roles => {
      res.render('auth/register', {
        errors,
        username,
        password,
        password2,
        roles
      });
    });

  } else {
    User.findOne({
        username: username
      })
      .then(user => {
        if (user) {
          errors.push({
            msg: 'Username already exists'
          });
          res.render('auth/register', {
            errors,
            role,
            username,
            password,
            password2
          });
        } else {
          console.log('create new user');
          let newUser = new User({
            username,
            password,
            role
          });

          bcrypt.genSalt(10, (err, salt) => {
            if (err) {
              console.log(err);
            }
            bcrypt.hash(newUser.password, salt, (err, hash) => {
              if (err) throw err;
              newUser.password = hash;
              newUser.save()
                .then(user => {
                  req.flash('success_msg', 'You are now registered and can log in');
                  res.redirect('/login');
                })
                .catch(err => console.log(err));
            });
          });
        }
      });
  }
});

router.post('/logout', ensureAuthenticated, (req, res, next) => {
  req.logout();
  req.flash('success_msg', 'You are logged out');
  res.redirect('/login');
});

router.get('/list-jobs', (req, res, next) => {

});

module.exports = router;