const express = require('express');
const router = express.Router();

// GET index
router.get('/', (req, res, next) => {
    res.render('employer/index', {title: 'Employer Index'});
});

module.exports = router;