const express = require('express');
const router = express.Router();
const User = require('../models/user');


module.exports = (passport) => {
    router.post('/signup', (req, res) => {
        User.findOne({username:req.body.username}, (err, doc) => {
            if (err) {
                res.status(500).send('error occured');
            } else {
                if(doc) {
                    res.status(500).send('username already exists');
                } else {
                    let user = new User();
                    user.username = username;
                    user.passport = user.hashPassword(password);
                    user.save((err,user) => {
                        if(err) {
                            res.status(500).send('db error');
                        } else {
                            res.send(user);
                        }
                    })
                }
            }
        });
    });

    router.post('/login',passport.authenticate('local',{
        failureRedirect:'/login',
        successRedirect:'/',
    }), (req, res) => {
        res.send('hey');
    });

    return router;
};