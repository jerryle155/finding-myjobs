const express = require('express');
const router = express.Router();
const Job = require('../models/job');
const Category = require('../models/category');
const { check, validationResult } = require('express-validator/check');
const { isEmployer } = require('../helpers/auth');
// const Gender = require('../models/job')

const {codes} = require('../helpers/job');

router.get('/', (req, res, next) => {
    res.redirect('/');
});

// GET - show job detail
router.get('/details/:id', (req, res, next) => {
    Job.findById(req.params.id).populate('category').exec((err, job) => {
        
        if (err) {
            // console.error('error, no job found!');
            return res.next(err);
        } else {
            if(!job) {
                return res.next(err);
            }
            // console.log(`Check ra: ${job}`);
            res.render('job/details', {
                title: job.title,
                job,
                user: req.user,
                // codes
            });
        }
    });

});

router.get('/create', isEmployer, (req, res, next) => {
    let errors = [];

    Category.find({}, (err, cates) => {
        if(err) {
            console.log(err);
        } else {
            if(!cates) {
                errors.push({msg: 'Cannot load categories, please check again'});
            }

            if(errors.length > 0) {
                res.render('index', {
                    errors
                });
            } else {
                // console.log(codes);
                res.render('job/create', {
                    title: 'Create new Job',
                    codes: codes,
                    cates: cates,
                    user: req.user
                });
            }
        }
    });
    
});

router.post('/store', isEmployer, (req, res, next) => {
    const {title,salary,level,qty,gender,job_type,employee_type,description,category} = req.body;
    let errors = [];

    if(!title || !salary || !level || !qty || !gender || !job_type || !employee_type || !description || !category) {
        errors.push({msg: 'Please enter all fields'});
    }

    if(errors.length > 0) {
        res.render('job/create', {
            errors, 
            title,
            salary,
            level,
            qty,
            gender,
            work_time,
            employee_type,
            description,
            category,
            user: req.user
        });
       
    } else {
        let job = new Job({
            title,
            salary,
            level,
            qty,
            gender,
            work_time: job_type,
            employee_type,
            description,
            category
        });

       
    
        // console.log(req.body.category);
    
        job.save()
        .then(async (job) => {
            // console.log(category);
            

            req.flash('success_msg', 'Your job was posted');
            res.redirect('/');
        })
        .catch(err => console.log(err));
           
    }

});

// GET - show edit form
router.get('/edit/:id', (req, res, next) => {
    Job.find({_id: req.params.id}, (err, doc) => {
        if(err) {
            console.log(err);
        } else {
            res.render('job/edit', {title: 'Edit Job', job: doc, codes});
        }
    });
});

router.post('/update/:id', (req, res, next) => {
    const {title,salary,level,qty,gender,job_type,employee_type,description,category} = req.body;
    let errors = [];

    if(!title || !salary || !level || !qty || !gender || !job_type || !employee_type || !description || !category) {
        errors.push({msg: 'Please enter all fields'});
    }

    if(errors.length > 0) {
        res.render('job/edit', {
            errors, 
            title,
            salary,
            level,
            qty,
            gender,
            job_type,
            employee_type,
            description,
            category
        });
       
    } else {
        let job = new Job({
            title,
            salary,
            level,
            qty,
            gender,
            job_type,
            employee_type,
            description,
            category
        });

       
    
        // console.log(req.body.category);
    
        job.updateOne()
        .then(job => {
            req.flash('success_msg', 'Your job was posted');
            res.redirect('/');
        })
        .catch(err => console.log(err));
           
    }
});

module.exports = router;