const express = require('express');
const router = express.Router();

// GET
router.get('/', (req, res, next) => {
    res.render('user/index', { title: 'Login Page' });
});

module.exports = router;