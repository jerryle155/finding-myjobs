const hbs = require('hbs');
// job helpers

hbs.registerHelper('getEmployeeTypeColor', (type) => {
  if(type == "Freelance") {
    return 'info';
  } else if(type == "Internship") {
    return 'danger';
  } else if(type == "Temporary") {
    return 'warning';
  } else {
    return 'success';
  }
});

const employeeTypes = ['Freelance', 'Internship', 'Temporary', 'Official'];
const jobLevels = ['High School', 'University', 'Doctor', 'Professor', 'Master', 'Other'];
const salaryRanges = ['Negotiable', 'From $500', 'Up to $1000', '$500 - $1000', '$1000 - $2000', 'Up to $2000', '$2000 - $3000', 'Up to $3000'];
const workTimeList = ['Full Time', 'Part Time'];
const genderList = ['Male', 'Female', 'Both'];

module.exports = {
  hbs,
  codes: {
    employeeTypes,
    jobLevels,
    salaryRanges,
    workTimeList,
    genderList
  }
};
