module.exports = {
    ensureAuthenticated: (req, res, next) => {
        if(req.isAuthenticated()) {
            return next();
        }
        req.flash('error_msg', 'Please log in to view that page');
        res.redirect('/login');
    },
    forwardAuthenticated: (req, res, next) => {
        if(!req.isAuthenticated()) {
            return next();
        }
        res.redirect('/');
    },
    isEmployer: (req, res, next) => {
        if(req.isAuthenticated() && req.user.role.name == "Employer") {
            return next();
        }
        req.flash('error_msg', 'You dont have permission to view that page');
        res.redirect('/');
    }
};