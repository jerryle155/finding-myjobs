const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');

const User = require('../models/user');

module.exports = (passport) => {
    passport.use(new LocalStrategy({usernameField: 'username'}, (username, password, done) => {
        // Match user
        User.findOne({username: username}).populate('role').then(user => {
            if(!user) {
                return done(null, false, {message: 'That user is not registered'});
            }

            console.log(user);

            // Match password
            bcrypt.compare(password, user.password, (err, isMatch) => {
                if(err) throw err;

                if(isMatch) {
                    return done(null, user);
                } else {
                    return done(null, false, {message: 'Password incorrect'});
                }
            });
        });
    }));

    passport.serializeUser((user, done) => {
        done(null, user.id);
    })

    passport.deserializeUser((id, done) => {
        User.findById(id)
            .populate('role')
            .exec( (err, user) => {
            done(err, user);
        });
    });
};