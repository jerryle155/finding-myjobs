const mongoose = require('mongoose');

// Job Schema
const categorySchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    flaticon: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    }
});

const Category = module.exports =  mongoose.model('Category', categorySchema);