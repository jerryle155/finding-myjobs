const mongoose = require('mongoose');

// Job Schema
const jobSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    salary: {
        type: String,
        required: true
    },
    level: {
        type: String,
        required: true
    },
    category: [{ 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Category' 
    }],
    qty: {
        type: Number,
        required: true
    },
    gender: {
        type: String,
        required: true
    },
    work_time: {
        type: String,
        required: true
    },
    employee_type: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    }
    
}, {timestamps: true});

const Job = module.exports =  mongoose.model('Job', jobSchema);